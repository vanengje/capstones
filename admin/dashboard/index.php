
<?php

      date_default_timezone_set("Asia/Manila");
      $current_year = date('Y');
      $current_month = date('m');
      $current_month_name = date('M');
      $current_day = date('d');
      $date_today = "$current_year"."-"."$current_month"."-"."$current_day";

      $query=mysqli_query($conn,"SELECT SUM(sales_total) AS stp FROM sales WHERE sales_date LIKE '%$date_today%'");
      $row=mysqli_fetch_array($query);

      $saletoday = (number_format($row['stp']));

      $mon = date('Y-m-d H:i:s',strtotime('monday this week'. '00:00:00'));
      $sun = date('Y-m-d H:i:s',strtotime('sunday this week'. '23:59:59'));

      $query4=mysqli_query($conn,"SELECT SUM(sales_total) AS stp FROM sales WHERE sales_date BETWEEN '$mon' AND '$sun'");
      $row4=mysqli_fetch_array($query4);

      $saleweekly = (number_format($row4['stp']));

      $query3=mysqli_query($conn,"SELECT SUM(sales_total) AS stp FROM sales WHERE sales_date LIKE '%$current_year"."-"."$current_month%'");
      $row3=mysqli_fetch_array($query3);

      $salemonthly = (number_format($row3['stp']));

      $query2=mysqli_query($conn,"SELECT SUM(sales_total) AS stp FROM sales WHERE sales_date LIKE '%$current_year"."-"."%'");
      $row2=mysqli_fetch_array($query2);

      $saleyearly = (number_format($row2['stp']));

?>
<meta charset="utf-8">
      <link rel="stylesheet" type="text/css" href="assets/icon/icofont/css/icofont.css">
      <link rel="stylesheet" type="text/css" href="assets/css/style.css">

<body>
<div id="wrapper">
<?php include('navbar.php'); ?>
<div style="height:50px;"></div>
<div id="page-wrapper" style="background-color: white;">
<div class="container-fluid">
<div class="page-body">
  <div class="row">

        <!-- order-card start -->
        <div class="col-md-6 col-xl-6">
            <div class="card bg-c-blue order-card">
                <div class="card-block">
                    <h6 class="m-b-20">Daily Sales</h6>
                    <h2 class="text-right"><i class="ti-shopping-cart f-left"></i><span><center>₱<?php echo $saletoday; ?></center></span></h2>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-6">
            <div class="card bg-c-green order-card">
                <div class="card-block">
                    <h6 class="m-b-20">Weekly Sales</h6>
                    <h2 class="text-right"><i class="ti-tag f-left"></i><span><center>₱<?php echo $saleweekly;?></center></span></h2>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-6">
            <div class="card bg-c-yellow order-card">
                <div class="card-block">
                    <h6 class="m-b-20">Monthly Sales</h6>
                    <h2 class="text-right"><i class="ti-reload f-left"></i><center>₱<?php echo $salemonthly;?></center></h2>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-6">
            <div class="card bg-c-pink order-card">
                <div class="card-block">
                    <h6 class="m-b-20">Yearly Sales</h6>
                    <h2 class="text-right"><i class="ti-wallet f-left"></i><span><center>₱<?php echo $saleyearly; ?></center></span></h2>
                </div>
            </div>
        </div>
    </div>
    <div>
        <?php

          $jan=mysqli_query($conn,"SELECT SUM(sales_total) AS stp FROM sales WHERE sales_date LIKE '%$current_year"."-"."01%'");
          $janrow=mysqli_fetch_array($jan);

          $jansale = $janrow['stp'];

          $feb=mysqli_query($conn,"SELECT SUM(sales_total) AS stp FROM sales WHERE sales_date LIKE '%$current_year"."-"."02%'");
          $febrow=mysqli_fetch_array($feb);

          $febsale = $febrow['stp'];

          $mar=mysqli_query($conn,"SELECT SUM(sales_total) AS stp FROM sales WHERE sales_date LIKE '%$current_year"."-"."03%'");
          $marrow=mysqli_fetch_array($mar);

          $marsale = $marrow['stp'];

          $apr=mysqli_query($conn,"SELECT SUM(sales_total) AS stp FROM sales WHERE sales_date LIKE '%$current_year"."-"."04%'");
          $aprrow=mysqli_fetch_array($apr);

          $aprsale = $aprrow['stp'];

          $may=mysqli_query($conn,"SELECT SUM(sales_total) AS stp FROM sales WHERE sales_date LIKE '%$current_year"."-"."05%'");
          $mayrow=mysqli_fetch_array($may);

          $maysale = $mayrow['stp'];

          $jun=mysqli_query($conn,"SELECT SUM(sales_total) AS stp FROM sales WHERE sales_date LIKE '%$current_year"."-"."06%'");
          $junrow=mysqli_fetch_array($jun);

          $junsale = $junrow['stp'];

          $jul=mysqli_query($conn,"SELECT SUM(sales_total) AS stp FROM sales WHERE sales_date LIKE '%$current_year"."-"."07'");
          $julrow=mysqli_fetch_array($jul);

          $julsale = $julrow['stp'];

          $aug=mysqli_query($conn,"SELECT SUM(sales_total) AS stp FROM sales WHERE sales_date LIKE '%$current_year"."-"."08%'");
          $augrow=mysqli_fetch_array($aug);

          $augsale = $augrow['stp'];

          $sept=mysqli_query($conn,"SELECT SUM(sales_total) AS stp FROM sales WHERE sales_date LIKE '%$current_year"."-"."09%'");
          $septrow=mysqli_fetch_array($sept);

          $septsale = $septrow['stp'];

          $oct=mysqli_query($conn,"SELECT SUM(sales_total) AS stp FROM sales WHERE sales_date LIKE '%$current_year"."-"."10%'");
          $octrow=mysqli_fetch_array($oct);

          $octsale = $octrow['stp'];

          $nov=mysqli_query($conn,"SELECT SUM(sales_total) AS stp FROM sales WHERE sales_date LIKE '%$current_year"."-"."11%'");
          $novrow=mysqli_fetch_array($nov);

          $novsale = $novrow['stp'];

          $dec=mysqli_query($conn,"SELECT SUM(sales_total) AS stp FROM sales WHERE sales_date LIKE '%$current_year"."-"."12%'");
          $decrow=mysqli_fetch_array($dec);

          $decsale = $decrow['stp'];

          $dataPoints = array( 
            array("y" => $jansale, "label" => "Jan"),
            array("y" => $febsale, "label" => "Feb"),
            array("y" => $marsale, "label" => "Mar"),
            array("y" => $aprsale, "label" => "Apr"),
            array("y" => $maysale, "label" => "May"),
            array("y" => $junsale, "label" => "Jun"),
            array("y" => $julsale, "label" => "Jul"),
            array("y" => $augsale, "label" => "Aug"),
            array("y" => $septsale, "label" => "Sept"),
            array("y" => $octsale, "label" => "Oct"),
            array("y" => $novsale, "label" => "Nov"),
            array("y" => $decsale, "label" => "Dec")
          );
           
          ?>

          <script>
              window.onload = function() {
               
              var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                theme: "light2",
                title:{
                  text: "Monthly Sales Graph"
                },
                axisY: {
                  title: "Profit Meter"
                },
                data: [{
                  type: "column",
                  yValueFormatString: "#,##0.## pesos",
                  dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                }]
              });
              chart.render();
               
              }
          </script>
          <br>
          <div id="chartContainer" style="height: 400px; width: 105%;"></div>
          <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
  <div class="modal-footer">
                	<button onclick="window.print();" class="btn btn-primary" id="print-btn">Print</button>
  
                </div>
    </div>
</div>
</div>
</div>
</div>
</div>

<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script type="text/javascript" src="assets/js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="assets/js/popper.js/popper.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap/js/bootstrap.min.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="assets/js/jquery-slimscroll/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="assets/js/modernizr/modernizr.js"></script>
<!-- am chart -->
<script src="assets/pages/widget/amchart/amcharts.min.js"></script>
<script src="assets/pages/widget/amchart/serial.min.js"></script>
<!-- Chart js -->
<script type="text/javascript" src="assets/js/chart.js/Chart.js"></script>
<!-- Todo js -->
<script type="text/javascript " src="assets/pages/todo/todo.js "></script>
<!-- Custom js -->
<script type="text/javascript" src="assets/pages/dashboard/custom-dashboard.min.js"></script>
<script type="text/javascript" src="assets/js/script.js"></script>
<script type="text/javascript " src="assets/js/SmoothScroll.js"></script>
<script src="assets/js/pcoded.min.js"></script>
<script src="assets/js/vartical-demo.js"></script>
<script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard3.js"></script>


<?php include('script.php'); ?>
<?php include('modal.php'); ?>
<?php include('add_modal.php'); ?>
<script src="custom.js"></script>
